//gulp-sass - компилятор SASS
//gulp-sourcemaps - отображение файла и номера строки в инспекторе стилей
//gulp-autoprefixer - добавление префиксов
//gulp-concat - объединение файлов
//gulp-clean-css - минификация кода
//gulp - сам gulp
//gulp-if - для определения стадии (разработка или релиз)
//browser-sync - автоперезагрузка страниц при изменении файлов

//Подключение модулей
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const cleanCss = require('gulp-clean-css');
const gulpIf = require('gulp-if');

const browserSync = require('browser-sync').create();

const config = {
    paths: {
        scss: './src/scss/**/*.scss',
        html: './dist/*.html'
    },
    output: {
        cssName: 'css/style.css',
        path: './dist'
    },
    isDevMode: true
};


//компиляция SASS
gulp.task('css', getStyles);
//создание сервера и наблюдение за изменениями
gulp.task('watch', watch);
//задача по-умолчанию      
gulp.task('default', gulp.series('css', 'watch'));


function getStyles() {
    return gulp.src(config.paths.scss)
            //добавление sourcemaps только в режиме разработки
            .pipe(gulpIf(config.isDevMode, sourcemaps.init()))
            .pipe(sass())
            .pipe(concat(config.output.cssName))
            .pipe(autoprefixer())
            //минификация для релиза
            .pipe(gulpIf(!config.isDevMode, cleanCss()))
            .pipe(gulpIf(config.isDevMode, sourcemaps.write()))
            //выходная папка
            .pipe(gulp.dest(config.output.path))
            //добавление в поток отслеживания
            .pipe(browserSync.stream());
}

//создание сервера и перезагрузка сервера при изменении файлов
function watch() {
    browserSync.init({
        server: {
            baseDir: config.output.path
        }
    });
    gulp.watch(config.paths.scss, getStyles);
    gulp.watch(config.paths.html).on('change', browserSync.reload);
}

