//Находим кнопку Меню
var navToggle = document.querySelector(".navbar-toggler");
// console.log(navToggle);

navToggle.addEventListener("click", setBlurBackground, false);

function setBlurBackground() {
    //Берём меняющийся параметр при клике на кнопку за основу. Это параметр Navbar Bootstrap 4.3.1
    var navAriaExpanded = navToggle.getAttribute("aria-expanded");
    // console.log(navAriaExpanded);

    //ищем все элементы со вспомогательным классом (данного класса может не быть в самих стилях)
    var blurElements = document.querySelectorAll(".js-blur-wrapper");
    if (navAriaExpanded == "false") {
        //если меню не раскрыто, значит оно будет раскрыто рано или поздно.

        //задаём новый стиль - фильтр с затемнением
        for (var i = 0; blurElements.length; i++) {
            blurElements[i].className="blur-effect";
        }
        console.log("blur is");

    }
    else {
        //если меню раскрыто, значит оно будет закрыто рано или поздно.
        //задаём новый стиль - возвращаем в исходное состояние
        for (var i = 0; blurElements.length; i++) {
            blurElements[i].className="js-blur-wrapper";
        }
        console.log("blur none");
    }
}




